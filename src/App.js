import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import Dashboard from './Components/Dashboard.jsx';
import Archive from './Components/Archive.jsx';
import User from './Components/User.jsx';
import Station from './Components/Station.jsx';
import Alert from './Components/Alert.jsx';
import Notification from './Components/Notification.jsx';
import Dashboard_loader from './Components/Dashboard_loader.jsx';
import Notification_loader from './Components/Notification_loader.jsx';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route exact path="/" component={Dashboard}/>
            <Route exact path="/dashboard_loader" component={Dashboard_loader}/>
            <Route exact path="/archive" component={Archive}/>
            <Route exact path="/user" component={User}/>
            <Route exact path="/station" component={Station}/>
            <Route exact path="/alert" component={Alert}/>
            <Route exact path="/notification" component={Notification}/>
            <Route exact path="/notification_loader" component={Notification_loader}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;