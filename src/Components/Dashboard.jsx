import React from 'react';
import { Layout, Row, Col, Button, Select, Divider, Icon, Input, Tabs, Breadcrumb, Dropdown, Menu, Slider, Table, Cascader, DatePicker } from 'antd';
import './dashboard.less';
import Head from './Head.jsx';
import Side from './Side.jsx';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

const marks = {
  0: '-19°C',
  100: '-12°C'
};

const { RangePicker } = DatePicker;

const TabPane = Tabs.TabPane;

function callback(key) {
	console.log(key);
}

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const config = {
	chart: {
		type: 'line',
		renderTo: 'chart',
    	margin: 0,
    	height: 150,
    	defaultSeriesType: 'areaspline'
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: 'Past 24 hr Trend'
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">Temp <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [-14.0, -11.5, -12.4, -15.2, -14.0, -16.0, -18.6, -18.5, -15.4, -15.8, -15.6, -14.4, -19.4, -17.9, -12.8, -12.78, -17.9, -13.8, -11.1, -18.9, -12.0, -12.1, -16.9, -14.4]
	}]
};

const { Content } = Layout;

const Option = Select.Option;

const filter = (
	<Menu>
		<Menu.Item key="chiller-1">Chiller-1</Menu.Item>
		<Menu.Item key="chiller-2">Chiller-2</Menu.Item>
		<Menu.Item key="chiller-3">Chiller-3</Menu.Item>
		<Menu.Item key="chiller-4">Chiller-4</Menu.Item>
		<Menu.Item key="chiller-5">Chiller-5</Menu.Item>
		<Menu.Item key="chiller-6">Chiller-6</Menu.Item>
	</Menu>
);

const activityColumn = [{
  title: 'Activity',
  dataIndex: 'activity',
  width: 150
}, {
  title: 'From',
  dataIndex: 'from',
  width: 150
}, {
  title: 'To',
  dataIndex: 'to',
  width: 150
}, {
  title: 'Duration',
  dataIndex: 'duration',
  align: 'center',
  width: 100
}];

const violationColumn = [{
  title: 'Date',
  dataIndex: 'date',
  width: 150,
}, {
  title: 'Violation',
  dataIndex: 'violation',
  width: 150,
}, {
  title: 'Min/Max',
  dataIndex: 'minmax',
  width: 100,
  align: 'center',
}, {
  title: 'Duration',
  dataIndex: 'duration',
  width: 100,
  align: 'center'
}];

const activityData = [{
	key: '1',
	activity: 'Online/Offline',
	from: "10:30, 22/10/2018",
	to: "12:30, 22/10/2018",
	duration: "2hrs",
}, {
	key: '2',
	activity: 'Online/Offline',
	from: "10:30, 22/10/2018",
	to: "12:30, 22/10/2018",
	duration: "2hrs",
}, {
	key: '3',
	activity: 'Maintenance',
	from: "10:30, 22/10/2018",
	to: "12:30, 22/10/2018",
	duration: "2hrs",
}, {
	key: '4',
	activity: 'Online/Offline',
	from: "10:30, 22/10/2018",
	to: "12:30, 22/10/2018",
	duration: "2hrs",
}, {
	key: '5',
	activity: 'Maintenance',
	from: "10:30, 22/10/2018",
	to: "12:30, 22/10/2018",
	duration: "2hrs",
}, {
	key: '6',
	activity: 'Maintenance',
	from: "10:30, 22/10/2018",
	to: "12:30, 22/10/2018",
	duration: "2hrs",
}];

const violationData = [];
for (let i = 0; i < 7; i++) {
  violationData.push({
    date: "10:30, 22/10/2018",
    violation: "Above -12 °C",
    minmax: "-22 °C",
    duration: "3hrs",
  });
}

const breadcrumb1 = [{
  value: 'city-1',
  label: 'City-1',
  children: [{
    value: 'location-1',
    label: 'Location-1',
    children: [{
      value: 'guesthouse-1',
      label: 'Guest House-1',
    }, {
      value: 'guesthouse-2',
      label: 'Guest House-2',
    }],
  }, {
    value: 'location-2',
    label: 'Location-2',
    children: [{
      value: 'guesthouse-3',
      label: 'Guest House-3',
    }, {
      value: 'guesthouse-4',
      label: 'Guest House-4',
    }],
  }],
}, {
  value: 'city-2',
  label: 'City-2',
  children: [{
    value: 'location-3',
    label: 'Location-3',
    children: [{
      value: 'guesthouse-5',
      label: 'Guest House-5',
    }],
  }],
}];

const breadcrumb2 = [{
  value: 'city-1',
  label: 'City-1',
  children: [{
    value: 'location-1',
    label: 'Location-1',
    children: [{
      value: 'guesthouse-1',
      label: 'Guest House-1',
      children: [{
      	value: 'chiller-1',
      	label: 'Chiller-1',
      }, {
      	value: 'chiller-2',
      	label: 'Chiller-2',
      }, {
      	value: 'chiller-3',
      	label: 'Chiller-3',
      }],
    }, {
      value: 'guesthouse-2',
      label: 'Guest House-2',
      children: [{
      	value: 'chiller-4',
      	label: 'Chiller-4',
      }, {
      	value: 'chiller-5',
      	label: 'Chiller-5',
      }],
    }],
  }, {
    value: 'location-2',
    label: 'Location-2',
    children: [{
      value: 'guesthouse-3',
      label: 'Guest House-3',
      children: [{
      	value: 'chiller-6',
      	label: 'Chiller-6',
      }, {
      	value: 'chiller-7',
      	label: 'Chiller-7',
      }, {
      	value: 'chiller-8',
      	label: 'Chiller-8',
      }],
    }, {
      value: 'guesthouse-4',
      label: 'Guest House-4',
      children: [{
      	value: 'chiller-9',
      	label: 'Chiller-9',
      }, {
      	value: 'chiller-10',
      	label: 'Chiller-10',
      }],
    }],
  }],
}, {
  value: 'city-2',
  label: 'City-2',
  children: [{
    value: 'location-3',
    label: 'Location-3',
    children: [{
      value: 'guesthouse-5',
      label: 'Guest House-5',
      children: [{
        value: 'chiller-11',
        label: 'Chiller-11',
      }],
    }],
  }],
}];

class Dashboard extends React.Component {

	constructor(props) {
		super(props);
		/**
		* This sets the initial state for the page.
		* @type {Object}
		*/
		this.state = {
			visible: true,
		};
		/**
		* This is used for bind the all stations data.
		* @type {object}
		*/
	}

	chillerDetails (){
		this.setState({
			visible: false,
		});
	};

	chiller (){
		this.setState({
			visible: true,
		});
	};

	render () {
		return (
			<div id="dashboard">
				<Side active_link="dashboard" />
				<Head/>
				<Layout>
					<Layout>
						{(() => {
							if (this.state.visible) {
								return <Content className="contains">
								<Cascader options={breadcrumb1} changeOnSelect className="bread-crumb filter-icon" defaultValue={['city-1', 'location-1', 'guesthouse-1']} />
								{/*<Breadcrumb className="bread-crumb">
									<Breadcrumb.Item href="">
										<Dropdown overlay={filter} trigger={['click']} placement="bottomLeft">
											<span className="filter-icon"/>
										</Dropdown>
									</Breadcrumb.Item>
									<Breadcrumb.Item>City-1</Breadcrumb.Item>
									<Breadcrumb.Item>Location-1</Breadcrumb.Item>
									<Breadcrumb.Item>Guest House-1</Breadcrumb.Item>
								</Breadcrumb>*/}
								<div className="contain">
									<Row className="rows" className="top-0">
										<div className="search">
											<Input placeholder="Search" prefix={<Icon type="search" />} />
										</div>
										<div className="select">
											<RangePicker
												className="calendar-icon"
										      	showTime={{ format: 'HH:mm' }}
										      	format="DD-MM-YYYY HH:mm"
										      	placeholder={['From', 'To']}
										    />
										</div>
									</Row>
									<Row className="legends">
										<span className="legend">
											<span className="ring success-border"></span>
											<span>Online</span>
										</span>
										<span className="legend">
											<span className="ring danger-border"></span>
											<span>Offline</span>
										</span>
										<span className="legend">
											<span className="ring deactive-border"></span>
											<span>Under Maintenance</span>
										</span>
										<span className="legend">
											<span className="within-range-icon"></span>
											<span>Within Range</span>
										</span>
										<span className="legend">
											<span className="violation-icon"></span>
											<span>Violation</span>
										</span>
									</Row>
									<Row>
										<div className="features">
											<div className="feature" onClick={() => this.chillerDetails()}>
												<div className="head-section">
													<span className="ring success-border"/>
													<span className="hed-txt">Chiller-1</span>
												</div>
												<div className="body-section feature-body">
													<Row className="value-contain">
														<span className="within-range-icon"></span>
														<span className="value">-16</span><span className="value"> °C</span>
													</Row>
													<Row className="range">
														<Slider className="within-range" defaultValue={50} disabled marks={marks} />
													</Row>
													<Row className="value-contain">
														<span className="warning-icon"></span>
														<span>Violations</span>
														<span className="violation-value">6</span>
													</Row>
												</div>
											</div>
											<div className="feature" onClick={() => this.chillerDetails()}>
												<div className="head-section">
													<span className="ring success-border"/>
													<span className="hed-txt">Chiller-2</span>
												</div>
												<div className="body-section feature-body">
													<Row className="value-contain">
														<span className="violation-icon"></span>
														<span className="value">-20</span><span className="value"> °C</span>
													</Row>
													<Row className="range">
														<Slider className="violation" defaultValue={100} disabled marks={marks} />
													</Row>
													<Row className="value-contain">
														<span className="warning-icon"></span>
														<span>Violations</span>
														<span className="violation-value">6</span>
													</Row>
												</div>
											</div>
											<div className="feature" onClick={() => this.chillerDetails()}>
												<div className="head-section">
													<span className="ring success-border"/>
													<span className="hed-txt">Chiller-3</span>
												</div>
												<div className="body-section feature-body">
													<Row className="value-contain">
														<span className="violation-icon"></span>
														<span className="value">-10</span><span className="value"> °C</span>
													</Row>
													<Row className="range">
														<Slider className="violation" defaultValue={0} disabled marks={marks} />
													</Row>
													<Row className="value-contain">
														<span className="warning-icon"></span>
														<span>Violations</span>
														<span className="violation-value">6</span>
													</Row>
												</div>
											</div>
											<div className="feature" onClick={() => this.chillerDetails()}>
												<div className="head-section">
													<span className="ring success-border"/>
													<span className="hed-txt">Chiller-4</span>
												</div>
												<div className="body-section feature-body">
													<Row className="value-contain">
														<span className="within-range-icon"></span>
														<span className="value">-16</span><span className="value"> °C</span>
													</Row>
													<Row className="range">
														<Slider className="within-range" defaultValue={50} disabled marks={marks} />
													</Row>
													<Row className="value-contain">
														<span className="warning-icon"></span>
														<span>Violations</span>
														<span className="violation-value">6</span>
													</Row>
												</div>
											</div>
										</div>
									</Row>
								</div>
							</Content>
						}
										
						else {
							return <Content className="contains chiller-details">
								<Cascader options={breadcrumb2} changeOnSelect className="bread-crumb filter-icon" defaultValue={['city-1', 'location-1', 'guesthouse-1', 'chiller-1']} />
								<div className="back-btn"><Button type="primary" icon="arrow-left" onClick={() => this.chiller()}>Back</Button></div>
								<Row className="head-container">
									<div className="head">
										<span className="ring success-border"/>
										<span>Chiller-1</span>
									</div>
									<div className="select">
										<RangePicker
											className="calendar-icon"
									      	showTime={{ format: 'HH:mm' }}
									      	format="DD-MM-YYYY HH:mm"
									      	placeholder={['From', 'To']}
									    />
									    <Button type="primary" icon="download">Download</Button>
									</div>
								</Row>
								<Row className="description">
									<span>This chiller is used for xyz purpose.</span>
								</Row>
								<Row className="graph-container" type="flex" justify="space-between">
									<Col className="white graphs" span={24}>
										<div className="head-section">
											<span className="within-range-icon"></span>
											<span>Temperature Trend</span>
											<span className="graph-hed">
												<span>Current Temperature: </span>
												<span className="cur-value">-16</span>
												<span> °C</span>
											</span>
										</div>
										<div className="body-section">
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</div>
									</Col>
								</Row>
								<Row className="legends">
									<span className="legend">
										<span className="ring success-border"></span>
										<span>Online</span>
									</span>
									<span className="legend">
										<span className="ring danger-border"></span>
										<span>Offline</span>
									</span>
									<span className="legend">
										<span className="ring deactive-border"></span>
										<span>Under Maintenance</span>
									</span>
									<span className="legend">
										<span className="within-range-icon"></span>
										<span>Within Range</span>
									</span>
									<span className="legend">
										<span className="violation-icon"></span>
										<span>Violation</span>
									</span>
									<span className="legend mar-left-30">
										<span>-19 °C</span>
										<span className="line-icon"></span>
										<span>-12 °C</span>
									</span>
								</Row>
								<Row gutter={35} className="mar-top-30 mar-bot-30">
									<Col span={12} className="violation-detail">
										<div className="head-section white">
											<span className="warning-icon"></span>
											<span>Violations</span>
											<span className="violation-value">6</span>
										</div>
										<div className="body-section white">
											<Table columns={violationColumn} dataSource={violationData} scroll={{ y: 250 }} />
										</div>
									</Col>
									<Col span={12} className="activity-detail">
										<div className="head-section white">
											<span className="activity-icon"></span>
											<span>Activities</span>
											<span className="activity-value">6</span>
										</div>
										<div className="body-section white">
											<Table columns={activityColumn} dataSource={activityData} scroll={{ y: 250 }} />
										</div>
									</Col>
								</Row>
							</Content>
							}
						})()}
					</Layout>
				</Layout>
			</div>
		);
	}
}

export default Dashboard;