import React, { Component } from 'react';
import './menu.less';
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon, Badge, Drawer, Modal, Button } from 'antd';

const { Header, Sider } = Layout;
const SubMenu = Menu.SubMenu;

const MenuItemGroup = Menu.ItemGroup;

class Side extends Component {

	constructor(props) {
		super(props);
		this.state = {
			collapsed: true,
			visible: false,
			modalVisible: false,
		};
	}

	onCollapse = (collapsed) => {
		this.setState({ collapsed });
	};

	showDrawer = () => {
		this.setState({
			visible: true,
		});
	};

	showModal = () => {
			this.setState({
				modalVisible: true,
			});
		}

	onClose = () => {
		this.setState({
			visible: false,
		});
	};

	handleClick = (e) => {
		console.log('click ', e);
		this.setState({
			current: e.key,
		});
	}

	handleCancel = () => {
    this.setState({ modalVisible: false });
  }

	render() {
		 const { modalVisible } = this.state;
	return (
		<Layout>
			<Modal
				visible={modalVisible}
				title=""
				onCancel={this.handleCancel}
				footer={[
					<div className="abt-footer">© 2018 Phoenix Robotix Pvt. Ltd.</div>
				]}
			>
				<div className="abt-modal">
					<img src="http://127.0.0.1:8080/datoms_logo.svg" className="datoms-logo"/>
					<div className="ver">Version 2.0.0</div>
				</div>
			</Modal>
			<Sider
				className="mobile-hidden1"
				collapsible
				onCollapse={this.onCollapse} style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0, zIndex: '11' }}
			>
				<div className="imgs">
					<img src="http://127.0.0.1:8080/datoms_logo.svg" />
				</div>
				<Menu theme="dark" mode="inline">
					<Menu.Item key="1" className={this.props.active_link == 'dashboard' ? 'ant-menu-item-selected' : ''}>
						<Link to='/'>
							<Icon type="layout" />
							<span>Dashboard</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="2" className={this.props.active_link == 'archive' ? 'ant-menu-item-selected' : ''}>
							<Link to='/archive'>
								<Icon type="file-text" />
								<span>Archive</span>
							</Link>
						</Menu.Item>
					<SubMenu
						key="settings"
						title={<span><Icon type="setting" /><span>Settings</span></span>}
					>
						<Menu.Item key="3" className={this.props.active_link == 'user' ? 'ant-menu-item-selected' : ''}>
							<Link to='/user' >
								<span>User</span>
							</Link>
						</Menu.Item>
						<Menu.Item key="4" className={this.props.active_link == 'station' ? 'ant-menu-item-selected' : ''}>
							<Link to='/station' >
								<span>Station</span>
							</Link>
						</Menu.Item>
						{/*<Menu.Item key="4" className={this.props.active_link == 'alert' ? 'ant-menu-item-selected' : ''}>
							<Link to='/alert' >
								<span>Alert</span>
							</Link>
						</Menu.Item>*/}
					</SubMenu>
					<SubMenu
						key="sub1"
						title={<span><Icon type="user" /><span>User</span></span>}
					>
						<Menu.Item key="5">Profile</Menu.Item>
						<Menu.Item key="6">Change Password</Menu.Item>
					</SubMenu>
					<Menu.Item key="7" className="back-trans">
						<Icon type="info-circle" />
						<span onClick={this.showModal}>About</span>
					</Menu.Item>
				</Menu>
			</Sider>
		</Layout>
			
	);
	}
}

export default Side;