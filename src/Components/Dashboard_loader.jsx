import React from 'react';
import { Layout, Row, Col, Skeleton, Spin, Icon } from 'antd';
import './dashboard.less';
import Head from './Head.jsx';
import Side from './Side.jsx';

const {Content} = Layout;

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} />;

class Dashboard_loader extends React.Component {

	constructor(props) {
		super(props);
		/**
		* This sets the initial state for the page.
		* @type {Object}
		*/
		this.state = {
			visible: true,
			visibleDetails: true,
		};
		/**
		* This is used for bind the all stations data.
		* @type {object}
		*/
	}

	render () {
		const { size } = this.state;

		return (
			<div id="dashboard" className="dashboard-type-1 type-1-loader">
				<Side active_link="dashboard" />
				<Head/>
				<Layout>
					<Layout>
						<Content className="contains">
							<div className="contain">
								<Row className="rows" className="top-0">
									<Col className="city-select" span={10}>
										<Skeleton active />
									</Col>
								</Row>
								<Row className="rows" type="flex" justify="space-around">
									<Col className="cols map-container" span={14}>
										<div className="full-height">
											<Spin indicator={antIcon} />
										</div>
									</Col>
									{(() => {
										if (this.state.visible) {
											return <Col className="cols width-100" span={8}>
												<Skeleton className="select-load" active />
												<div className="device-details">
													<Row type="flex" justify="space-around" className="device-row">
														<Skeleton className="list-load" active />
													</Row>
													<Row type="flex" justify="space-around" className="device-row">
														<Skeleton className="list-load" active />
													</Row>
													<Row type="flex" justify="space-around" className="device-row">
														<Skeleton className="list-load" active />
													</Row>
													<Row type="flex" justify="space-around" className="device-row">
														<Skeleton className="list-load" active />
													</Row>
												</div>
											</Col>
										}
										
										else {
											return <Col className="cols width-100" span={8}>
												<Row className="border-bot">
													<Col className="hed" span={17}><Skeleton className="hed-load" active /></Col>
												</Row>
												<div className="aqi-trend">
													<Row type="flex" gutter={6} justify="center" align="bottom" className="pad-20 pad-lr-10">
														<Skeleton className="avtar-load" active avatar />
													</Row>
													<Row className="hr24-graph">
														<Skeleton className="avtar-load" active avatar />
													</Row>
													<div className="aqi-scale"><Skeleton className="list-load" active /></div>
												</div>
												<div className="more"><Spin indicator={antIcon} /></div>
											</Col>
										}
									})()}	
								</Row>
							</div>
						</Content>
					</Layout>

					{(() => {
						if (this.state.visibleDetails) {
							return <div>
								<Layout>
									<Content className="contains">
										<div className="contain aqi-details">
											<Row style={{margin: "0 0 20px 0"}}>
												<Col span={12}><span className="head"><Skeleton className="hed-load" active /></span></Col>
											</Row>
											<Row type="flex" justify="space-around">
												<Col className="white parameter-trend display-none" span={23}>
													<div className="section-head">
														<span><Skeleton className="hed-load" active /></span>
													</div>
													<Row className="aqi-pollutant-graphs">
														<Skeleton className="avtar-load" active avatar />
													</Row>
													<Row className="aqi-pollutant-graphs">
														<Skeleton className="avtar-load" active avatar />
													</Row>
													<Row className="aqi-pollutant-graphs">
														<Skeleton className="avtar-load" active avatar />
													</Row>
												</Col>
											</Row>
										</div>
									</Content>
								</Layout>

								<Layout>
									<Content className="contains">
										<div className="contain aqi-details">
											<Row type="flex" justify="space-around">
												<Col span={11} className="white top-0">
													<div className="section-head"><span><Skeleton className="hed-load" active /></span></div>
													<Row type="flex" justify="space-around" align="bottom" className="pad-20 pad-bot-40">
														<Col span={11}>
															<Skeleton className="avtar-load" active avatar />
														</Col>
														<Col span={11}>
															<Skeleton className="avtar-load" active avatar />
														</Col>
													</Row>
												</Col>
												<Col className="white parameter-trend mar-bot-25" span={11}>
													<div className="section-head">
														<span><Skeleton className="hed-load" active /></span>
													</div>
													<Row type="flex" justify="space-around" className="pad-bot-40">
														<Spin className="wind-load" indicator={antIcon} />
													</Row>
												</Col>
											</Row>
										</div>
									</Content>
								</Layout>
								<Layout className="display-none">
									<Content className="contains">
										<div className="contain mar-top-35 graph-load">
											<Spin indicator={antIcon} />
										</div>
									</Content>
								</Layout>
							</div>
						}
					})()}	
				</Layout>
			</div>
		);
	}
}

export default Dashboard_loader;