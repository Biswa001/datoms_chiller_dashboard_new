const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
  // do stuff with the webpack config...
 config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config);

 config = rewireLess.withLoaderOptions({
 	javascriptEnabled: true,
 	modifyVars: { "@primary-color": "#FF7D3F" },
 	})(config, env);
  return config;
};